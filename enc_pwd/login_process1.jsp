<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CA2 pwd demo process</title>
</head>
<body>
<%
String uname = request.getParameter("uname");
String pwd = request.getParameter("pwd");
out.println(uname +","  + pwd);

//load driver
	Class.forName("com.mysql.jdbc.Driver");
	//out.println("driver loaded");
	
	// prepare URL
	// db = p0123456
	// u/n and pwd= p0123456
	String connURL ="jdbc:mysql://localhost/p0123456?user=p0123456&password=p0123456"; 
	
	// get connection
	Connection conn = DriverManager.getConnection(connURL);
	//out.println("conn started");
	
	// create statement
	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM member WHERE name=? AND password=?");
	
	// prepare sql and excute
	stmt.setString(1, uname);
	stmt.setString(2, pwd);
	   
	out.println(stmt);
 	ResultSet rs = stmt.executeQuery();
 	
 	// process the result
 	// go to 1st row if exist
 	if(rs.next()){
 		// this person exist in database
 		// login success
 		out.println("success");
 		// store a session variable to indicate login done
 		session.setAttribute ("login","2a03");
 	}else{
 		out.println("get out of here!");
 	}
 	
 	conn.close();
%>
</body>
</html>