<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Restricted Access page</title>
</head>
<body>
<%
// Retrieve the session attribute LOGIN-STATUS and check:
//	If LOGIN-STATUS is null, redirect the user to login.jsp
// NEED to cast to String using (String)
// as good as telling java dont worry it's a string
String status = (String)session.getAttribute("LOGIN-STATUS");
if(status == null){
	// hey hacker, nice try. go login first. 
	response.sendRedirect("login.jsp");
}
// think of how to REUSE this code, but please not copy paste to ALL pages. 
%>
<h1>Restricted page</h1>
</body>
</html>