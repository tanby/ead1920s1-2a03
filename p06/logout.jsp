<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Logout page</title>
</head>
<body>
<h1>Logout</h1>
<%
// kill session, logout
session.invalidate();
%>
</body>
</html>