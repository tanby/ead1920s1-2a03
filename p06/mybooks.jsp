<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="ead.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Book Store</title>
</head>
<body>
<h1>ArrayList demo</h1>
<%
// Create a Value Bean class Book with the following attributes:
/* -	ISBN  (String)
-	Title (String)
-	Author (String)
-	Publisher (String)
-	Quantity (int)
-	Price (double)
 */
 
// -	Create 3 Books objects
Book b1 = new Book();
// use default value for b1
// ASSUME there's a form to let user to enter title and quantity
// but for DEMO purpose we set thru hard code
Book b2 = new Book();
b2.setTitle("EHD how to hack");
b2.setQuantity(21);
Book b3 = new Book();
b3.setTitle("SMW securing windows is it possible?");
b3.setQuantity(11);

// -	Store these objects inside a ArrayList
// empty array list
ArrayList<Book> books = new ArrayList<Book>();
// add the 3 books
// ASSUME no form, so add manually for this DEMO
books.add(b1);
books.add(b2);
books.add(b3);

// -	Retrieve all items stored in the ArrayList and 
// display them on the web-browser
for(int i = 0; i<books.size(); i++){
	Book b = books.get(i);
	// print it out
	out.println("<div>" +b.getTitle() + ": " + b.getQuantity() + "</div>");
}

// for DEMO only, clear array list
//books.clear();


// PART 3: Store the ArrayList in the Session [use book as the attribute name]
session.setAttribute("book", books);
%>
</body>
</html>