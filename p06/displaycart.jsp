<%@page import="java.util.ArrayList, ead.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Shopping Cart</title>
</head>
<body>
<h1>Shopping Cart</h1>
<%
// Retrieve the ArrayList that you stored in the session attribute book
ArrayList<Book> bs = (ArrayList<Book>)session.getAttribute("book");
for(int i = 0; i<bs.size(); i++){
	Book b = bs.get(i);
	// print it out
	out.println("<div>" +b.getTitle() + ": " + b.getQuantity() + "</div>");
}
%>
</body>
</html>