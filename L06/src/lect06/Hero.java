package lect06;

public class Hero {
	private int idhero;
	private String name;
	private int power;
	/**
	 * @return the idhero
	 */
	public int getIdhero() {
		return idhero;
	}
	/**
	 * @param idhero the idhero to set
	 */
	public void setIdhero(int idhero) {
		this.idhero = idhero;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the power
	 */
	public int getPower() {
		return power;
	}
	/**
	 * @param power the power to set
	 */
	public void setPower(int power) {
		this.power = power;
	}
	
}
