package com.spgames;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet("/Authenticate")
public class Authenticate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Authenticate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		// since we are sending the form thru POST so add code here
		String u = request.getParameter("username");
		
		// print out to test first
		// THIS is new in term 2. cos jsp has "out" built in
		PrintWriter out = response.getWriter();
		
		//out.println("username: " + u);
		// If user keyed in the right credentials:
		// DO ON YOUR OWN: the checking with MySQL DB
		// simulate a simple if in this demo
		// assume abc is correct username
		if(u.equals("abc")) {
			// Create a Session attribute USER and 
			// store it with the entered loginid
			HttpSession session = request.getSession();
			session.setAttribute("USER", u);
			
			// Redirect him/her to another jsp
			response.sendRedirect("p07/restricted.jsp");
		}else {
			// redirect back to login
			response.sendRedirect("p07/login.html");
		}
		

	}

}
